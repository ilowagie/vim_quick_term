" quick-term
"   A Vim plugin by Inigo Lowagie (inigo@lowagie.com)

if exists('g:loaded_quickTerm') | finish | endif

let s:save_cpo = &cpo
set cpo&vim

function InitBuffer(bufNr)
    exe "buffer " . a:bufNr
    " set buffer name
    file "quickTermScratchBuf"
    " In this buffer, we'll allow the user to exit Terminal mode with Esc
    tnoremap <buffer> <Esc> <C-\><C-n>
    let hidden='hide'
    " go back to the buffer that we started with
    bf
endfunction

function ToggleQuickTerm()
    let l:curBufNr = bufnr()
    if (l:curBufNr == g:quickTermScratchBufNr)
        return ':hide'
    else
        return 'bo 15sp | b ' . g:quickTermScratchBufNr . ' | normal A'
    endif
endfunction

" create a buffer for the scratch terminal and initialize it
let g:quickTermScratchBufNr = bufadd('term://bash')
exe InitBuffer(g:quickTermScratchBufNr)

" add keymapping for toggle
exe "nnoremap " . get(g:, 'quickTermOpenKey', '<leader>tt') . " :exe ToggleQuickTerm()<Enter>"

let &cpo = s:save_cpo
unlet s:save_cpo

let g:loaded_quickTerm = 1
